Somos uma empresa de suplementos e alimentos, com atendimento 100% digital, comprometida com a qualidade na escolha das melhores matérias-primas do mercado, certificadas pelos órgãos nacionais e internacionais disponibilizando laudos técnicos para que todos os nossos clientes se certifiquem da qualidade dos produtos que estão adquirindo.

Sabemos que o nosso corpo perde diariamente nutrientes que nem sempre são compensados na alimentação, por isso estamos atentos às necessidades dos nossos clientes, oferecendo um mix de produtos que irá ajudar repor essas perdas diárias. Visando sempre o melhor para os melhores seguimos com transparência e respeito a todos os nossos clientes amigos.

Atendimento
Seg. à Sex. das 9h às 17h

SAC
Itu - São Paulo
(11) 9.8760-0362
Email: contato@makainutri.com.br
